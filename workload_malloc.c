#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <sys/time.h>
#include <sys/resource.h>

#define TOTAL_SIZE	4000000000l	// 4 GB
// #define DEBUG

struct stat_t {
	long total;
	long rss_diff;
	long overhead;
	double overhead_percent;
	double elapsed_time;
};

///////////////////////////////////////////////////////////////////////////////

#define start_timer() ( { \
	struct timespec ts; \
	clock_gettime( CLOCK_REALTIME, &ts ); \
	ts; \
} )

#define get_elapsed_time(start_time) ( { \
	struct timespec end_time = start_timer(); \
	double elapsed_time = end_time.tv_sec - start_time.tv_sec; \
	elapsed_time += 1.0e-9 * ( end_time.tv_nsec - start_time.tv_nsec ); \
	elapsed_time; \
} )

///////////////////////////////////////////////////////////////////////////////

#define WORKLOAD_BEGIN(num) \
	struct rusage usage[ 2 ]; \
	struct timespec start_time; \
	struct stat_t stat; \
	long i, count, size; \
	char **ptrs = ( char ** ) malloc( (num) * sizeof( char ** ) ); \
	\
	memset( ptrs, 0, (num) * sizeof( char ** ) ); \
	memset( &stat, 0, sizeof( struct stat_t ) ); \
	getrusage( RUSAGE_SELF, usage )

#define WORKLOAD_END() \
	getrusage( RUSAGE_SELF, usage + 1 ); \
 	\
 	stat.total = TOTAL_SIZE; \
	stat.rss_diff = ( usage[ 1 ].ru_maxrss - usage[ 0 ].ru_maxrss ) * 1024; \
	stat.overhead = stat.rss_diff - stat.total; \
	if ( stat.overhead < 0 ) stat.overhead = 0; \
	stat.overhead_percent = ( double ) stat.overhead / stat.total * 100.0; \
	free( ptrs );\
 	\
	return stat

///////////////////////////////////////////////////////////////////////////////

struct stat_t workload1() {
	WORKLOAD_BEGIN( TOTAL_SIZE / 100 );

	// ------------------------------------------------------------------------
	// Allocate 100 bytes
	size = 100;
	count = TOTAL_SIZE / size;

	start_time = start_timer();
	for( i = 0; i < count; i++ ) {
		ptrs[ i ] = ( char * ) malloc( sizeof( char ) * size );
		memset( ptrs[ i ], 0, sizeof( char ) * size );
	}

#ifdef DEBUG
	printf( "%s(): Allocated %ld x %ld bytes\n", __func__, size, count );
#endif

	for( i = 0; i < count; i++ ) {
		free( ptrs[ i ] );
	}

	stat.elapsed_time = get_elapsed_time( start_time );
	// ------------------------------------------------------------------------

	WORKLOAD_END();
}

///////////////////////////////////////////////////////////////////////////////

struct stat_t workload2_3_4( double fraction ) {
	long tmp, index;
#ifdef DEBUG
	long freed;
#endif
	WORKLOAD_BEGIN( 2 * TOTAL_SIZE / 100 );

	// ------------------------------------------------------------------------
	// Allocate 100 bytes
	size = 100;
	count = TOTAL_SIZE / size;

	start_time = start_timer();
	for( i = 0; i < count; i++ ) {
		ptrs[ i ] = ( char * ) malloc( sizeof( char ) * size );
		memset( ptrs[ i ], 0, sizeof( char ) * size );
	}

#ifdef DEBUG
	printf( "%s(): Allocated %ld x %ld bytes\n", __func__, size, count );
	freed = 0;
#endif

	// Free 10/50/90% of chunks
	tmp = count * fraction;
	srand( time( NULL ) );
	for ( i = 0; i < tmp; i++ ) {
		while( 1 ) {
			index = rand() % count;
			if ( ptrs[ index ] ) {
				free( ptrs[ index ] );
				ptrs[ index ] = NULL;
#ifdef DEBUG
				freed++;
#endif
				break;
			}
		}
	}

#ifdef DEBUG
	printf( "%s():     Freed %ld x %ld bytes\n", __func__, size, freed );
#endif

	// Allocate 130 bytes
	tmp = tmp * size / 130; // We can have $tmp more chunks!
	size = 130;
	for( i = 0; i < tmp; i++ ) {
		ptrs[ count + i ] = ( char * ) malloc( sizeof( char ) * size );
		memset( ptrs[ count + i ], 0, sizeof( char ) * size );
	}

#ifdef DEBUG
	printf( "%s(): Allocated %ld x %ld bytes\n", __func__, size, tmp );
	freed = 0;
#endif

	for( i = 0; i < count * 2; i++ ) {
		if ( ptrs[ i ] )
			free( ptrs[ i ] );
	}

	stat.elapsed_time = get_elapsed_time( start_time );
	// ------------------------------------------------------------------------

	WORKLOAD_END();
}

///////////////////////////////////////////////////////////////////////////////

struct stat_t workload5_16( double fraction, int init_range_start, int init_range_end, int final_range_start, int final_range_end ) {
	long total_size, more_size, tmp, index;
#ifdef DEBUG
	long freed;
#endif
	WORKLOAD_BEGIN( 2 * TOTAL_SIZE / 100 );

	// ------------------------------------------------------------------------
	// Allocate $init_range_start-$init_range_end bytes
	count = 0;
	total_size = 0;

	srand( time( NULL ) );

	start_time = start_timer();
	for( i = 0; TOTAL_SIZE - total_size > 0; i++ ) {
		size = rand() % ( init_range_end - init_range_start + 1 ) + init_range_start;
		if ( total_size + size > TOTAL_SIZE )
			break;
		ptrs[ i ] = ( char * ) malloc( sizeof( char ) * size );
		memset( ptrs[ i ], 0, sizeof( char ) * size );
		*( ( long * ) ptrs[ i ] ) = size;
		count++;
		total_size += size;
	}

#ifdef DEBUG
	printf( "%s(): Allocated %ld bytes / %ld chunks\n", __func__, total_size, count );
	freed = 0;
#endif

	// Free 10/50/90% of chunks
	tmp = total_size * fraction;
	for ( i = 0; tmp > 0; i++ ) {
		while( 1 ) {
			index = rand() % count;
			if ( ptrs[ index ] ) {
				tmp -= *( ( long * ) ptrs[ index ] );
				free( ptrs[ index ] );
				ptrs[ index ] = NULL;
#ifdef DEBUG
				freed++;
#endif
				break;
			}
		}
	}

#ifdef DEBUG
	printf( "%s():     Freed %ld bytes / %ld chunks\n", __func__, ( long ) ( total_size * fraction - tmp ), freed );
#endif

	// Allocate 200-250 bytes	
	tmp = total_size * fraction - tmp; // We can allocate $tmp more bytes!
	total_size -= tmp;
	more_size = 0;
	count = 0;
	for( i = 0; TOTAL_SIZE - total_size > 0; i++ ) {
		size = rand() % ( final_range_end - final_range_start + 1 ) + final_range_start;
		if ( total_size + size > TOTAL_SIZE )
			break;
		ptrs[ count + i ] = ( char * ) malloc( sizeof( char ) * size );
		memset( ptrs[ count + i ], 0, sizeof( char ) * size );
		*( ( long * ) ptrs[ count + i ] ) = size;
		count++;
		total_size += size;
		more_size += size;
	}

#ifdef DEBUG
	printf( "%s(): Allocated %ld more bytes / %ld more chunks\n", __func__, more_size, count );
#endif

	for( i = 0; i < count * 2; i++ ) {
		if ( ptrs[ i ] )
			free( ptrs[ i ] );
	}

	stat.elapsed_time = get_elapsed_time( start_time );
	// ------------------------------------------------------------------------

	WORKLOAD_END();
}

///////////////////////////////////////////////////////////////////////////////

int main( int argc, char **argv ) {
	char *endptr;
	long workload;
	struct stat_t stat;

	// Parse arguments
	// ---------------
	if ( argc < 2 )
		goto error;

	workload = strtol( argv[ 1 ], &endptr, 10 );
	if ( endptr == argv[ 1 ] )
		goto error;

	switch( workload ) {
		case  1: stat = workload1(); break;
		case  2: stat = workload2_3_4( 0.1 ); break;
		case  3: stat = workload2_3_4( 0.5 ); break;
		case  4: stat = workload2_3_4( 0.9 ); break;
		case  5: stat = workload5_16( 0.1, 100, 150, 200, 250 ); break;
		case  6: stat = workload5_16( 0.5, 100, 150, 200, 250 ); break;
		case  7: stat = workload5_16( 0.9, 100, 150, 200, 250 ); break;
		case  8: stat = workload5_16( 0.1, 100, 200, 1000, 2000 ); break;
		case  9: stat = workload5_16( 0.5, 100, 200, 1000, 2000 ); break;
		case 10: stat = workload5_16( 0.9, 100, 200, 1000, 2000 ); break;
		case 11: stat = workload5_16( 0.1, 1000, 2000, 1500, 2500 ); break;
		case 12: stat = workload5_16( 0.5, 1000, 2000, 1500, 2500 ); break;
		case 13: stat = workload5_16( 0.9, 1000, 2000, 1500, 2500 ); break;
		case 14: stat = workload5_16( 0.1, 50, 150, 5000, 15000 ); break;
		case 15: stat = workload5_16( 0.5, 50, 150, 5000, 15000 ); break;
		case 16: stat = workload5_16( 0.9, 50, 150, 5000, 15000 ); break;
		default: goto error;
	}

	// Print statistics
	// ----------------
#ifdef DEBUG
	printf( "Workload\tRSS Inc.\tOverhead\tOverhead %%\tElapsed Time\n" );
#endif
	printf( "%ld\t%ld\t%ld\t%8.6lf%%\t%.9lf\n", workload, stat.rss_diff, stat.overhead, stat.overhead_percent, stat.elapsed_time );

	return 0;

	// Usage
	// -----
error:
	fprintf( stderr, "Usage: %s [Workload No.]\n\n", argv[ 0 ] );
	fprintf( stderr, "Workload | %27s | Delete | %28s\n", "Before", "After" );
	fprintf( stderr, "-------- | --------------------------- | ------ | ----------------------------\n" );
	fprintf( stderr, "   W%2d   | %27s |   %3s  | %28s\n", 1, "Fixed 100 bytes", "N/A", "N/A" );
	fprintf( stderr, "   W%2d   | %27s |   %3s  | %28s\n", 2, "Fixed 100 bytes", "10%", "Fixed 130 bytes" );
	fprintf( stderr, "   W%2d   | %27s |   %3s  | %28s\n", 3, "Fixed 100 bytes", "50%", "Fixed 130 bytes" );
	fprintf( stderr, "   W%2d   | %27s |   %3s  | %28s\n", 4, "Fixed 100 bytes", "90%", "Fixed 130 bytes" );
	fprintf( stderr, "   W%2d   | %27s |   %3s  | %28s\n", 5, "Uniform 100 - 150 bytes", "10%", "Uniform 200 - 250 bytes" );
	fprintf( stderr, "   W%2d   | %27s |   %3s  | %28s\n", 6, "Uniform 100 - 150 bytes", "50%", "Uniform 200 - 250 bytes" );
	fprintf( stderr, "   W%2d   | %27s |   %3s  | %28s\n", 7, "Uniform 100 - 150 bytes", "90%", "Uniform 200 - 250 bytes" );
	fprintf( stderr, "   W%2d   | %27s |   %3s  | %28s\n", 8, "Uniform 100 - 200 bytes", "10%", "Uniform 1,000 - 2,000 bytes" );
	fprintf( stderr, "   W%2d   | %27s |   %3s  | %28s\n", 9, "Uniform 100 - 200 bytes", "50%", "Uniform 1,000 - 2,000 bytes" );
	fprintf( stderr, "   W%2d   | %27s |   %3s  | %28s\n", 10, "Uniform 100 - 200 bytes", "90%", "Uniform 1,000 - 2,000 bytes" );
	fprintf( stderr, "   W%2d   | %27s |   %3s  | %28s\n", 11, "Uniform 1,000 - 2,000 bytes", "10%", "Uniform 1,500 - 2,500 bytes" );
	fprintf( stderr, "   W%2d   | %27s |   %3s  | %28s\n", 12, "Uniform 1,000 - 2,000 bytes", "50%", "Uniform 1,500 - 2,500 bytes" );
	fprintf( stderr, "   W%2d   | %27s |   %3s  | %28s\n", 13, "Uniform 1,000 - 2,000 bytes", "90%", "Uniform 1,500 - 2,500 bytes" );
	fprintf( stderr, "   W%2d   | %27s |   %3s  | %28s\n", 14, "Uniform 50 - 150 bytes", "10%", "Uniform 5,000 - 15,000 bytes" );
	fprintf( stderr, "   W%2d   | %27s |   %3s  | %28s\n", 15, "Uniform 50 - 150 bytes", "50%", "Uniform 5,000 - 15,000 bytes" );
	fprintf( stderr, "   W%2d   | %27s |   %3s  | %28s\n", 16, "Uniform 50 - 150 bytes", "90%", "Uniform 5,000 - 15,000 bytes" );
	fprintf( stderr, "\nNote: The value printed is in bytes.\n" );
	return 1;
}
