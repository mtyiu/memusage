#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <sys/time.h>
#include <sys/resource.h>

#define MAXIMUM_SIZE	1000000

///////////////////////////////////////////////////////////////////////////////

#define start_timer() ( { \
	struct timespec ts; \
	clock_gettime( CLOCK_REALTIME, &ts ); \
	ts; \
} )

#define get_elapsed_time(start_time) ( { \
	struct timespec end_time = start_timer(); \
	double elapsed_time = end_time.tv_sec - start_time.tv_sec; \
	elapsed_time += 1.0e-9 * ( end_time.tv_nsec - start_time.tv_nsec ); \
	elapsed_time; \
} )

///////////////////////////////////////////////////////////////////////////////

int main( int argc, char **argv ) {
	long i, size, count, total, rss_diff, overhead;
	struct rusage usage[ 2 ];
	struct timespec start_time;
	double elapsed_time;
	char *ptrs[ MAXIMUM_SIZE ];
	char *endptr;

	// Parse arguments
	// ---------------
	if ( argc < 3 )
		goto error;

	size = strtol( argv[ 1 ], &endptr, 10 );
	if ( endptr == argv[ 1 ] || size > MAXIMUM_SIZE )
		goto error;

	count = strtol( argv[ 2 ], &endptr, 10 );
	if ( endptr == argv[ 2 ] )
		goto error;

	// Prepare pointer buffer
	// ----------------------
	getrusage( RUSAGE_SELF, usage );

	// Allocate memory
	// ---------------
	start_time = start_timer();
	for( i = 0; i < count; i++ ) {
		ptrs[ i ] = new char[ size ];
		memset( ptrs[ i ], 0, sizeof( char ) * size );
	}
	elapsed_time = get_elapsed_time( start_time );
	getrusage( RUSAGE_SELF, usage + 1 );

	// Free memory
	// -----------
	for( i = 0; i < count; i++ ) {
		delete[] ptrs[ i ];
	}

	// Print statistics
	// ----------------
	total = size * count;
	rss_diff = ( usage[ 1 ].ru_maxrss - usage[ 0 ].ru_maxrss ) * 1024;
	overhead = rss_diff - total;
	if ( overhead < 0 )
		overhead = 0;
	printf( "Size\tCount\tTotal\tRSS Inc.\tOverhead\tOverhead %%\tElapsed Time\n" );
	printf( "%ld\t%ld\t%ld\t%ld\t%ld\t%8.6lf%%\t%.9lf\n", size, count, total, rss_diff, overhead, ( double ) overhead / total * 100, elapsed_time );

	return 0;

	// Usage
	// -----
error:
	fprintf( stderr, "Usage: %s [Size] [Count]\n", argv[ 0 ] );
	fprintf( stderr, "\nNote: The value printed is in bytes.\n" );
	return 1;
}
