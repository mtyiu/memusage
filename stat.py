#!/usr/bin/env python3

import sys, re, math, statistics

overhead = {}
overhead_all = {}
elapsed_time = {}
elapsed_time_all = {}
line_regex = re.compile( '(\d+)\t(\d+)\t(\d+)\t(\d+)\t(\d+)\t([\d.]+\%)\t([\d.]+)' )

if len( sys.argv ) != 3:
	sys.stderr.write( 'Usage: %s [Log Filename] [Output directory]\n' %( sys.argv[ 0 ] ) )
	sys.exit( 1 )

try:
	f = open( sys.argv[ 1 ] )
except IOError:
	sys.stderr.write( 'Error: The log file cannot be found!\n' )
	sys.exit( 1 )

first = True
for line in f:
	if first:
		first = False
	else:
		ret = line_regex.search( line ).groups()
		size = ret[ 0 ]
		count = ret[ 1 ]
		if size not in overhead:
			overhead[ size ] = {}
			overhead_all[ size ] = {}
			elapsed_time[ size ] = {}
			elapsed_time_all[ size ] = {}
		if count not in overhead[ size ]:
			overhead[ size ][ count ] = 0
			overhead_all[ size ][ count ] = []
			elapsed_time[ size ][ count ] = 0.0
			elapsed_time_all[ size ][ count ] = []
		overhead_all[ size ][ count ].append( float( ret[ 4 ] ) / ( float( size ) * float( count ) ) )
		elapsed_time_all[ size ][ count ].append( float( ret[ 6 ] ) )

f.close()

for size in overhead.keys():
	for count in overhead[ size ].keys():
		# Remove extreme values
		# overhead_all[ size ][ count ] = sorted( overhead_all[ size ][ count ] )
		# overhead_all[ size ][ count ] = overhead_all[ size ][ count ][ 5:15 ]
		overhead[ size ][ count ] = ( float( sum( overhead_all[ size ][ count ] ) ) / 20 ) * 100.0 # 100 is to convert the fraction into percentage...

		# elapsed_time_all[ size ][ count ] = sorted( elapsed_time_all[ size ][ count ] )
		# elapsed_time_all[ size ][ count ] = elapsed_time_all[ size ][ count ][ 5:15 ]
		elapsed_time[ size ][ count ] = math.fsum( elapsed_time_all[ size ][ count ] ) / 20.0

for size in overhead.keys():
	f = open( sys.argv[ 2 ] + '/' + size, 'w' )
	it = iter( sorted( overhead[ size ].keys() ) )
	for count in it:
		f.write(
			'%s\t%lf\t%lf\t%lf\t%lf\t%lf\t%lf\t%lf\t%lf\t%lf\t%lf\t%lf\t%lf\n'
			%( count,
			   overhead[ size ][ count ], # average
			   statistics.median( overhead_all[ size ][ count ] ) * 100, # statistics.median
			   min( overhead_all[ size ][ count ] ) * 100, # box_min
			   statistics.median( overhead_all[ size ][ count ][ 0:10 ] ) * 100, # whisker_min
			   statistics.median( overhead_all[ size ][ count ][ 10:20 ] ) * 100, # whisker_high
			   max( overhead_all[ size ][ count ] ) * 100, # box_high

			   elapsed_time[ size ][ count ], # average
			   statistics.median( elapsed_time_all[ size ][ count ] ), # statistics.median
			   min( elapsed_time_all[ size ][ count ] ), # box_min
			   statistics.median( elapsed_time_all[ size ][ count ][ 0:10 ] ), # whisker_min
			   statistics.median( elapsed_time_all[ size ][ count ][ 10:20 ] ), # whisker_high
			   max( elapsed_time_all[ size ][ count ] )  # box_high
			)
		)

	f.close()
