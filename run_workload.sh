#!/bin/bash

touch log/workload_malloc.log
for i in {1..16}; do
	echo "Workload #$i"
	for j in {1..20}; do
		echo -e "\tIteration #$j"
		sudo bash <<<'sync && echo 3 > /proc/sys/vm/drop_caches'
		./workload_malloc $i | tail -n1 >> log/workload_malloc.log
	done
done

echo '########################################################################'

touch log/workload_new.log
for i in {1..16}; do
	echo "Workload #$i"
	for j in {1..20}; do
		echo -e "\tIteration #$j"
		sudo bash <<<'sync && echo 3 > /proc/sys/vm/drop_caches'
		./workload_new $i | tail -n1 >> log/workload_new.log
	done
done
