CC=gcc
CPP=g++
CFLAGS=-Wall -O3

all: malloc new workload_malloc workload_new

malloc: malloc.c
	$(CC) $(CFLAGS) -o $@ $<

new: new.cpp
	$(CPP) $(CFLAGS) -o $@ $<

workload_malloc: workload_malloc.c
	$(CC) $(CFLAGS) -o $@ $<

workload_new: workload_new.cpp
	$(CPP) $(CFLAGS) -o $@ $<

clean:
	rm -f malloc new workload_malloc workload_new
