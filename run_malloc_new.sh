#!/bin/bash

for size in 10 100 1000 10000 100000; do
	for count in 10 100 1000 10000 10000 20000 30000 40000 50000 60000 70000 75000; do
		for i in {1..20}; do
			echo "Size = $size; Count = $count; Iteration #$i"
			sudo bash <<<'sync && echo 3 > /proc/sys/vm/drop_caches'
			./malloc $size $count | tail -n1 >> log/malloc.log
			./new $size $count | tail -n1 >> log/new.log
		done
	done
done
