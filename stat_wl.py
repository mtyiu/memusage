#!/usr/bin/env python3

import sys, re, math, statistics

overhead = [ {}, {} ]
overhead_all = [ {}, {} ]
elapsed_time = [ {}, {} ]
elapsed_time_all = [ {}, {} ]
line_regex = re.compile( '^(\d+)\t(\d+)\t(\d+)\t([\d.]+)\%\t([\d.]+)$' )

if len( sys.argv ) != 4:
	sys.stderr.write( 'Usage: %s [Log Filename 1] [Log Filename 2] [Output filename]\n' %( sys.argv[ 0 ] ) )
	sys.exit( 1 )

for i in [ 0, 1 ]:
	try:
		f = open( sys.argv[ i + 1 ] )
	except IOError:
		sys.stderr.write( 'Error: The log file cannot be found!\n' )
		sys.exit( 1 )

	first = True
	for line in f:
		if first:
			first = False
		else:
			ret = line_regex.search( line ).groups()
			workload = ret[ 0 ]
			if workload not in overhead[ i ]:
				overhead[ i ][ workload ] = 0
				overhead_all[ i ][ workload ] = []
				elapsed_time[ i ][ workload ] = 0.0
				elapsed_time_all[ i ][ workload ] = []
			overhead_all[ i ][ workload ].append( float( ret[ 3 ] ) )
			elapsed_time_all[ i ][ workload ].append( float( ret[ 4 ] ) )

	f.close()

	for workload in overhead[ i ].keys():
		# Remove extreme values
		# overhead_all[ i ][ workload ] = sorted( overhead_all[ i ][ workload ] )
		# overhead_all[ i ][ workload ] = overhead_all[ i ][ workload ][ 5:15 ]
		overhead[ i ][ workload ] = math.fsum( overhead_all[ i ][ workload ] ) / 20 # 100 is to convert the fraction into percentage...

		# elapsed_time_all[ i ][ workload ] = sorted( elapsed_time_all[ i ][ workload ] )
		# elapsed_time_all[ i ][ workload ] = elapsed_time_all[ i ][ workload ][ 5:15 ]
		elapsed_time[ i ][ workload ] = math.fsum( elapsed_time_all[ i ][ workload ] ) / 20.0

f = open( sys.argv[ 3 ], 'w' )

for workload in iter( sorted( overhead[ 0 ].keys() ) ):
	f.write(
		'%s\t%lf\t%lf\t%lf\t%lf\n'
		%( workload,
		   overhead[ 0 ][ workload ],
		   overhead[ 1 ][ workload ],
		   elapsed_time[ 0 ][ workload ],
		   elapsed_time[ 1 ][ workload ]
		)
	)
	# f.write(
	# 	'%s\t%lf\t%lf\t%lf\t%lf\t%lf\t%lf\t%lf\t%lf\t%lf\t%lf\t%lf\t%lf\t%lf\t%lf\t%lf\t%lf\t%lf\t%lf\t%lf\t%lf\t%lf\t%lf\t%lf\t%lf\n'
	# 	%( workload,
	# 	   # Log file 1 #
	# 	   overhead[ 0 ][ workload ], # average
	# 	   statistics.median( overhead_all[ 0 ][ workload ] ), # statistics.median
	# 	   min( overhead_all[ 0 ][ workload ] ), # box_min
	# 	   statistics.median( overhead_all[ 0 ][ workload ][ 0:10 ] ), # whisker_min
	# 	   statistics.median( overhead_all[ 0 ][ workload ][ 10:20 ] ), # whisker_high
	# 	   max( overhead_all[ 0 ][ workload ] ), # box_high

	# 	   elapsed_time[ 0 ][ workload ], # average
	# 	   statistics.median( elapsed_time_all[ 0 ][ workload ] ), # statistics.median
	# 	   min( elapsed_time_all[ 0 ][ workload ] ), # box_min
	# 	   statistics.median( elapsed_time_all[ 0 ][ workload ][ 0:10 ] ), # whisker_min
	# 	   statistics.median( elapsed_time_all[ 0 ][ workload ][ 10:20 ] ), # whisker_high
	# 	   max( elapsed_time_all[ 0 ][ workload ] ),  # box_high

	# 	   # Log file 2 #
	# 	   overhead[ 1 ][ workload ], # average
	# 	   statistics.median( overhead_all[ 1 ][ workload ] ), # statistics.median
	# 	   min( overhead_all[ 1 ][ workload ] ), # box_min
	# 	   statistics.median( overhead_all[ 1 ][ workload ][ 0:10 ] ), # whisker_min
	# 	   statistics.median( overhead_all[ 1 ][ workload ][ 10:20 ] ), # whisker_high
	# 	   max( overhead_all[ 1 ][ workload ] ), # box_high

	# 	   elapsed_time[ 1 ][ workload ], # average
	# 	   statistics.median( elapsed_time_all[ 1 ][ workload ] ), # statistics.median
	# 	   min( elapsed_time_all[ 1 ][ workload ] ), # box_min
	# 	   statistics.median( elapsed_time_all[ 1 ][ workload ][ 0:10 ] ), # whisker_min
	# 	   statistics.median( elapsed_time_all[ 1 ][ workload ][ 10:20 ] ), # whisker_high
	# 	   max( elapsed_time_all[ 1 ][ workload ] )
	# 	)
	# )

f.close()
